import React, { useState } from 'react';

function App() {
  const [cashPayment, setCashPayment] = useState(0);
  const [cardPayment, setCardPayment] = useState(0);
  
  const handleCashChange = (event) => {
    setCashPayment(event.target.value);
  };
  
  const handleCardChange = (event) => {
    setCardPayment(event.target.value);
  };
  
  const handleSubmit = (event) => {
    event.preventDefault();
    const totalPayment = parseInt(cashPayment) + parseInt(cardPayment);
    alert(`Total payment: ${totalPayment} rupees`);
  };
  
  return (
    <div>
      <h2>Payment</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Cash payment:
          <input type="number" value={cashPayment} onChange={handleCashChange} />
        </label>
        <br />
        <label>
          Card payment:
          <input type="number" value={cardPayment} onChange={handleCardChange} />
        </label>
        <br />
        <button type="submit">Pay</button>
      </form>
    </div>
  );
}

export default App;
